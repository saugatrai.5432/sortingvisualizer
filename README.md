## Sorting Visualizer

Sorting Visualizer is a web application built with Angular that provides visual representations of various sorting algorithms. This tool is designed to help users understand how different sorting algorithms work by visualizing their steps in real-time.

## Demo

You can find the demo by navigating to this link [Demo link](https://sortingvisualizer-saugatrai-5432-274d3befaa6df9588fd50230a0e4ff.gitlab.io/).

## Features

-   **Multiple Sorting Algorithms:** Visualize popular sorting algorithms such as Bubble Sort, Selection Sort, Merge Sort, and Quick Sort.
-   **Customizable Array Size:** Adjust the size of the array to be sorted.
-   **Speed Control:** Control the speed of the visualization to observe the sorting process at different paces.
-   **Responsive Design:** A user-friendly interface that works across various devices and screen sizes.

## Installation

To get started with the Sorting Visualizer, follow these steps:

1. **Clone the repository**

> `git clone https://gitlab.com/saugatrai.5432/sortingvisualizer.git` > `cd sorting-visualizer`

2. **Install dependencies**

> `npm install`

3. **Run the application**

> `ng serve`

4. **Open your browser**

Navigate to http://localhost:4200/ to see the application in action.

## Usage

1. **Select Algorithm:** Choose the sorting algorithm you want to visualize from the options available.
2. **Generate Array:** Click on the "Generate New Array" button to create a new array with random values.
3. **Adjust Settings:** Use the sliders to adjust the array size and the speed of the visualization.
4. **Start Sorting:** Click the "Sort" button to start the visualization. Use the "Pause" button to pause and the "Step" button to go through the algorithm step-by-step.

## Available Sorting Algorithms

1. **Bubble Sort:** A simple comparison-based algorithm where each pair of adjacent elements is compared and swapped if they are in the wrong order.
2. **Selection Sort:** Divides the input into a sorted and an unsorted region, repeatedly selecting the smallest (or largest) element from the unsorted region and moving it to the sorted region.
3. **Merge Sort:** A divide-and-conquer algorithm that divides the array into smaller subarrays, sorts them, and then merges them back together.
4. **Quick Sort:** Another divide-and-conquer algorithm that selects a pivot element and partitions the array around the pivot, recursively sorting the subarrays.

## Contributing

Contributions are welcome! If you have any suggestions or improvements, please create a pull request or submit an issue.

## Fork the repository

1. Create your feature branch (git checkout -b feature/your-feature)
2. Commit your changes (git commit -m 'Add some your-feature')
3. Push to the branch (git push origin feature/your-feature)
4. Open a merge request
