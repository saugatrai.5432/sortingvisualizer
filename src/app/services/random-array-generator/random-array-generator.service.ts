import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RandomArrayGeneratorService {
  constructor() {}

  generateNewArray(range: number) {
    const randomArray = [];
    let randomNo = Math.random();
    for (let i = 0; i < range; i++) {
      if (randomNo < 0.03){
          randomNo += 0.03;
      }
      randomArray.push(randomNo);
      randomNo = Math.random();
    }
    return randomArray;
  }
}
