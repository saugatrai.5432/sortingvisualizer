import { Injectable } from '@angular/core';
import { ColorService } from '../color/color.service';
import { DataControlService } from '../data-control/data-control.service';

@Injectable({
    providedIn: 'root',
})
export class MergeSortService {
    speed: number;
    constructor(
        private dataControlService: DataControlService,
        private colorService: ColorService
    ) {
        this.speed = 5;
    }

    async sort() {
        let array = this.dataControlService.getCurrentArray();
        const speedSub = this.dataControlService.currentSpeed.subscribe(
            (currentSpeed) => {
                this.speed = currentSpeed as number;
            }
        );
        await this.mergeSort(array, 0, array.length - 1);
        speedSub.unsubscribe();
    }

    async mergeSort(arr: number[], left: number, right: number) {
        if (left < right) {
            const middle = Math.floor(left + (right - left) / 2);
            await this.mergeSort(arr, left, middle);
            await this.mergeSort(arr, middle + 1, right);

            await this.merge(arr, left, middle, right);
        }
    }

    async merge(arr: number[], left: number, middle: number, right: number) {
        let i = 0,
            j = 0,
            k = left;
        let lower = arr.slice(left, middle + 1);
        let upper = arr.slice(middle + 1, right + 1);
        while (i < lower.length && j < upper.length) {
            if (lower[i] <= upper[j]) {
                arr[k] = lower[i];
                this.colorService.setColors([
                    { index: i, color: 'red' },
                    { index: k, color: 'red' },
                ]);
                i++;
            } else {
                this.colorService.setColors([
                    { index: j, color: 'green' },
                    { index: k, color: 'green' },
                ]);
                arr[k] = upper[j];
                j++;
            }
            k++;
            await this.sleep(this.speed);
        }
        this.colorService.resetColors();

        while (i < lower.length) {
            arr[k] = lower[i];
            i++;
            k++;
            await this.sleep(this.speed);
        }

        while (j < upper.length) {
            arr[k] = upper[j];
            j++;
            k++;
            await this.sleep(this.speed);
        }
    }

    async sleep(milliseconds: number) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(true);
            }, milliseconds);
        });
    }
}
