export interface Theme {
    name: string;
    properties: any;
  }

export const light: Theme = {
    name: "light",
    properties: {
        '--primary-background-color': '#e0e0e0',
        '--header-color': '#041C32',
        '--bar-color': '#65068a',
        '--primary-btn-color': '#f5f5f5',
        '--font-color': 'black'
    }
};

export const dark: Theme = {
    name: "dark",
    properties: {
        '--primary-background-color': '#24202e',
        '--header-color': '#040D12',
        '--bar-color': '#9481fa',
        '--primary-btn-color': '#764bee',
        '--font-color': 'white'
    }
};