import { Component, OnInit } from '@angular/core';
import { SortingService } from '../../services/sorting/sorting.service';
import { SortingAlgorithms } from '../../models/algorithms.enum';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { dark } from 'src/app/services/theme/theme';
import { DataControlService } from 'src/app/services/data-control/data-control.service';
import { RandomArrayGeneratorService } from 'src/app/services/random-array-generator/random-array-generator.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
    arrayLength = 50;
    algorithms = [
        SortingAlgorithms.BubbleSort,
        SortingAlgorithms.SelectionSort,
        SortingAlgorithms.QuickSort,
        SortingAlgorithms.MergeSort,
    ];
    showSelectAlgo = false;
    sortingAlgorithms = SortingAlgorithms;
    selectedAlgorithm = this.algorithms[0];
    sorting = false;

    constructor(
        private ragService: RandomArrayGeneratorService,
        private dcService: DataControlService,
        private sortingService: SortingService,
        private themeService: ThemeService
    ) {}

    ngOnInit(): void {
        this.generateNewArray();
        this.sortingService.isSorting.subscribe((sorting) => {
            this.sorting = sorting;
        });
    }

    isDarkTheme() {
        return this.themeService.getActiveTheme() == dark;
    }

    changeRange(event: any) {
        this.arrayLength = event.srcElement.value;
        this.generateNewArray();
    }

    changeSpeed(event: any) {
        const speed = 150 - event.srcElement?.value;
        this.dcService.changeSpeed(speed);
    }

    generateNewArray() {
        const randomArray = this.ragService.generateNewArray(this.arrayLength);
        this.dcService.changeData(randomArray);
        this.sortingService.updateArray([1, 2]);
    }

    chnageAlog(algo: SortingAlgorithms) {
        this.selectedAlgorithm = algo;
        this.showSelectAlgo = false;
        this.sortingService.changeAlgorithm(algo);
    }

    sort() {
        if (!this.sorting) {
            this.sortingService.sort();
        }
    }

    toggleTheme() {
        if (this.themeService.isDarkTheme()) {
            this.themeService.setLightTheme();
        } else {
            this.themeService.setDarkTheme();
        }
    }
}
